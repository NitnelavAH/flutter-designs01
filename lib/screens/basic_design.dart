import 'package:flutter/material.dart';

class BasicDesignScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: const [
          Image(
            image: AssetImage('assets/landscape.jpeg'),
          ),
          Title(),
          ButtonSection(),
          TextContainer()
        ],
      ),
    );
  }
}

class Title extends StatelessWidget {
  const Title({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'Tutorial de diseños flutter',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                'Fernando Herrera',
                style: TextStyle(color: Colors.black45),
              ),
            ],
          ),
          const Expanded(child: SizedBox()),
          const Icon(Icons.star, color: Colors.red),
          const Text('41')
        ],
      ),
    );
  }
}

class ButtonSection extends StatelessWidget {
  const ButtonSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          CustomButton(icon: Icons.call, iconText: 'CALL'),
          CustomButton(icon: Icons.route, iconText: 'ROUTE'),
          CustomButton(icon: Icons.share, iconText: 'SHARE'),
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    required this.icon,
    required this.iconText,
  }) : super(key: key);

  final IconData icon;
  final String iconText;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        children: [
          Icon(
            icon,
            color: Colors.blueAccent,
          ),
          const SizedBox(height: 5),
          Text(
            iconText,
            style: const TextStyle(color: Colors.blueAccent),
          )
        ],
      ),
      onTap: () => print(iconText),
    );
  }
}

class TextContainer extends StatelessWidget {
  const TextContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: const Text(
          'Elit tempor do officia nostrud officia anim. Veniam do enim consequat non elit elit dolore ea excepteur nostrud proident eiusmod mollit. Dolore Lorem consectetur aliqua nisi nisi fugiat consequat ea velit id. Velit enim officia ipsum ipsum ea ut. Laboris voluptate pariatur fugiat consectetur non.'),
    );
  }
}
